<?php

if (!defined('XOOPS_ROOT_PATH')) {
	die('XOOPS root path not defined');
}
$modversion['dirname'] = _MI_NAME_MODULE;
$modversion['name'] = _MI_NAME_MODULE;
$modversion['version'] = '0.2';
$modversion['description'] = _MI_DESC_MODULE;
$modversion['author'] = 'JMColeman';
$modversion['credits'] = 'hyperclock | www.voyatrax.org';
$modversion['help'] = 'http://www.voyatrax.org/modules/newbb';
$modversion['license'] = 'GPLv3 see LICENSE';
$modversion['official'] = '0';
$modversion['image'] = 'images/vtlegals_logo.png';

// Admin
$modversion['hasAdmin'] = '0'; // is planned.

// Menu
$modversion['hasMain'] = '1';
//Submenus
$modversion['sub'][0]['name'] = _MI_MODULE_NAME1;
$modversion['sub'][0]['url'] = 'terms.php';
$modversion['sub'][1]['name'] = _MI_MODULE_NAME2;
$modversion['sub'][1]['url'] = 'privacy.php';
$modversion['sub'][2]['name'] = _MI_MODULE_NAME3;
$modversion['sub'][2]['url'] = 'imprint.php';

?>
