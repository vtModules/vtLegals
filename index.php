<?php

include("../../mainfile.php");
include(XOOPS_ROOT_PATH."/header.php");

$meta_keywords = "vtCMS, legal, legalinfo, vtlegals, imprint, privacy, policy, terms";
$meta_description = "vtLegals is a module to represent website legal info.";
$pagetitle = "vtLegals";

if(isset($xoTheme) && is_object($xoTheme)) {
    $xoTheme->addMeta( 'meta', 'keywords', $meta_keywords);
    $xoTheme->addMeta( 'meta', 'description', $meta_description);
} else {    // Compatibility for old Xoops versions
    $xoopsTpl->assign('xoops_meta_keywords', $meta_keywords);
    $xoopsTpl->assign('xoops_meta_description', $meta_description);
}

$xoopsTpl->assign('xoops_pagetitle', $pagetitle);

//this will only work if your theme is using this smarty variables
$xoopsTpl->assign( 'xoops_showlblock', 1); //set to 0 to hide left blocks
$xoopsTpl->assign( 'xoops_showrblock', 1); //set to 0 to hide right blocks
$xoopsTpl->assign( 'xoops_showcblock', 1); //set to 0 to hide center blocks
?>

<!-- HTML CONTENT-->
<table width="100%" cellspacing="1" class="outer">
  <tr>
    <th><?php echo _MI_NAME_MODULE; ?></th>
  </tr>
  <tr>
    <td class="odd" style="text-align:left;"><font size='+1'><ul>
		<li><a href='terms.php'><?php echo _MI_MODULE_DESC1; ?></a></li><br>
		<li><a href='privacy.php'><?php echo  _MI_MODULE_DESC2; ?></a></li><br>
		<li><a href='imprint.php'><?php echo _MI_MODULE_DESC3; ?></a></li><br>
    </font></ul></td>
  </tr>
</table>
<br>
<table width="100%" cellspacing="0" class="outer">
  <tr>
    <td class="foot" style="font-weight:normal;" align="right"><span class="itemPoster"><?php echo _MI_COPYRIGHT; ?></span></td>
  </tr>
</table>
<br>
<!-- END HTML CONTENT-->
<?php
include(XOOPS_ROOT_PATH."/footer.php");
?>
