<?php
// GERMAN
// ------------------------- Lang Info -------------------------------\\
// main.php is for the module lang point defines.
// admin.php is for the Admin lang defines.
// modinfo.php is for the xoops_version.php and preferences lang defines.
// blocks.php is for the Block lang defines 
//
// There are also some naming standards that are good to follow.
// like _AM_MODULE_LANGVAR or something similar.
//
// AM means Admin
// MI means modinfo
// MB means blocks
// MD means main
//
// _MODULE_ should be something unique to identify your module short abbrev.
//
// -------------------------------------------------------------------\\

// Terms
define('_MD_MODULE_NAME1','Nutzungbedingungen');
define('_MD_MODULE_DESC1','Nutzungbedingungen');
#define('_MD_MESSAGE1','Nutzungbedingungen wird hier hinzugefügt.');

// Privacy
define('_MD_MODULE_NAME2','Datenschutz');
define('_MD_MODULE_DESC2','Datenschutz-Bestimmungen');
#define('_MD_MESSAGE2','Datenschutz-Bestimmungen wird hier hinzugefügt.');

// Imprint
define('_MD_MODULE_NAME3','Impressum');
define('_MD_MODULE_DESC3','Impressum');
#define('_MD_MESSAGE3','');

?>
