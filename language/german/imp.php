<?php
// GERMAN
// ------------------------- Lang Info -------------------------------\\
// main.php is for the module lang point defines.
// admin.php is for the Admin lang defines.
// modinfo.php is for the xoops_version.php and preferences lang defines.
// blocks.php is for the Block lang defines 
//
// There are also some naming standards that are good to follow.
// like _AM_MODULE_LANGVAR or something similar.
//
// AM means Admin
// MI means modinfo
// MB means blocks
// MD means main
//
// _MODULE_ should be something unique to identify your module short abbrev.
//
// -------------------------------------------------------------------\\

### NOTE: This has been added differently. This is module specific.
// Imprint
define('_VTX_IMP_MSG','<div class="impressum"><h1>Impressum</h1><p>Angaben gemäß § 5 TMG</p><p>Max Muster <br> 
Musterweg<br> 
12345 Musterstadt <br> 
</p><p> <strong>Vertreten durch: </strong><br>
Max Muster<br>
</p><p><strong>Kontakt:</strong> <br>
Telefon: 01234-789456<br>
Fax: 1234-56789<br>
E-Mail: <a href="mailto:max@muster.de">max@muster.de</a></br></p><p><strong>Umsatzsteuer-ID: </strong> <br>
Umsatzsteuer-Identifikationsnummer gemäß §27a Umsatzsteuergesetz: Musterustid.<br><br>
<strong>Wirtschafts-ID: </strong><br>
Musterwirtschaftsid<br>
</p><p><strong>Aufsichtsbehörde:</strong><br>
Musteraufsicht Musterstadt<br></p><p><strong>Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:</strong><br>
Max Muster <br> 
Musterweg<br> 
12345 Musterstadt <br></p> 
<br> 
Website Impressum erstellt durch <a href="http://www.impressum-generator.de">impressum-generator.de</a>
 </div>');
?>
