<?php
// GERMAN
// ------------------------- Lang Info -------------------------------\\
// main.php is for the module lang point defines.
// admin.php is for the Admin lang defines.
// modinfo.php is for the xoops_version.php and preferences lang defines.
// blocks.php is for the Block lang defines 
//
// There are also some naming standards that are good to follow.
// like _AM_MODULE_LANGVAR or something similar.
//
// AM means Admin
// MI means modinfo
// MB means blocks
// MD means main
//
// _MODULE_ should be something unique to identify your module short abbrev.
//
// -------------------------------------------------------------------\\

// Module Info
define('_MI_NAME_MODULE','vtLegals');
define('_MI_DESC_MODULE','vtLegals ist ein Modul für das Hinzufügen Ihrer rechtliche Informationen. Wie Nutzungsbedingungen, eine Datenschutzrichtlinie oder Ihren Impressum.');
define('_MI_COPYRIGHT','vtLegals v1.0 - Copyright <a href="http://www.voyatrax.org" target="blank">the VoyaTrax CMS Project</a>');

// Terms
define('_MI_MODULE_NAME1','Nutzungbedingungen');
define('_MI_MODULE_DESC1','Nutzungbedingungen');
define('_MI_MESSAGE1','Nutzungbedingungen');
// Privacy
define('_MI_MODULE_NAME2','Datenschutz');
define('_MI_MODULE_DESC2','Datenschutz-Bestimmungen');
define('_MI_MESSAGE2','Datenschutz');
// Imprint
define('_MI_MODULE_NAME3','Impressum');
define('_MI_MODULE_DESC3','Impressum');
define('_MI_MESSAGE3','Impressum');
?>
