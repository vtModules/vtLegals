<?php
// ENGLISH

// Privacy
define('_VTX_PP_MSG',"<p><{$xoops_sitename}> - <{$xoops_slogan}> (<strong><{$xoops_sitename}></strong>) operates several websites including <{$xoops_url}>. It is <{$xoops_sitename}>'s policy to respect your privacy regarding any information we may collect while operating our websites.</p>

<h3>Website Visitors</h3>

<p>Like most website operators, <{$xoops_sitename}> collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. <{$xoops_sitename}>'s purpose in collecting non-personally identifying information is to better understand how <{$xoops_sitename}>'s visitors use its website. From time to time, <{$xoops_sitename}> may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.</p>

<p><{$xoops_sitename}> also collects potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments. <{$xoops_sitename}> only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below, except that blog commenter IP addresses are visible and disclosed to the administrators of the blog where the comment was left.</p>

<h3>Gathering of Personally-Identifying Information</h3>

<p><{$xoops_sitename}> discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on <{$xoops_sitename}>'s behalf or to provide services available at <{$xoops_sitename}>'s websites, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using <{$xoops_sitename}>'s websites, you consent to the transfer of such information to them. <{$xoops_sitename}> will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, <{$xoops_sitename}> discloses potentially personally-identifying and personally-identifying information only in response to a subpoena, court order or other governmental request, or when <{$xoops_sitename}> believes in good faith that disclosure is reasonably necessary to protect the property or rights of <{$xoops_sitename}>, third parties or the public at large. If you are a registered user of an <{$xoops_sitename}> website and have supplied your email address, <{$xoops_sitename}> may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what's going on with <{$xoops_sitename}> and our products. We primarily use our various product blogs to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. <{$xoops_sitename}> takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.</p>

<h3>Cookies</h3>

<p>A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. <{$xoops_sitename}> uses cookies to help <{$xoops_sitename}> identify and track visitors, their usage of <{$xoops_sitename}> website, and their website access preferences. <{$xoops_sitename}> visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using <{$xoops_sitename}>'s websites, with the drawback that certain features of <{$xoops_sitename}>'s websites may not function properly without the aid of cookies.</p>

<h3>Business Transfers</h3>

<p>If <{$xoops_sitename}>, or substantially all of its assets, were acquired, or in the unlikely event that <{$xoops_sitename}> goes out of business or enters bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of <{$xoops_sitename}> may continue to use your personal information as set forth in this policy.</p>

<h3>Ads</h3>

<p>Ads appearing on any of our websites may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by <{$xoops_sitename}> and does not cover the use of cookies by any advertisers.</p>

<h3>Privacy Policy Changes</h3>

<p>Although most changes are likely to be minor, <{$xoops_sitename}> may change its Privacy Policy from time to time, and in <{$xoops_sitename}>'s sole discretion. <{$xoops_sitename}> encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p>

<p>This Privacy Policy document is an adapation of the Privacy Policy on <a href='http://automattic.com' target='_blank'>Automattic.com</a>, available under a <a href='http://creativecommons.org/licenses/by-sa/3.0/' target='_blank'>Creative Commons Sharealike</a> license.</p>");

?>
