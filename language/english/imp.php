<?php
// ENGLISH

// Imprint
define('_VTX_IMP_MSG','<div class="impressum"><h1>Imprint</h1><p>According to § 5 TMG</p><p>John Doe <br> 
Smith Main<br> 
12345 City <br> 
</p><p> <strong>Represented by: </strong><br>
John Doe<br>
</p><p><strong>Contact:</strong> <br>
Phone: 01234-789456<br>
Fax: 1234-56789<br>
E-Mail: <a href="mailto:max@muster.de">max@muster.de</a></br></p><p><strong>Tax ID: </strong> <br>
Sales tax identification number according to §27a sales tax law: Musterustid.<br><br>
<strong>Economic id: </strong><br>
Musterwirtschaftsid<br>
</p><p><strong>Authority:</strong><br>
Pattern supervision model city<br></p><p><strong>Responsible for the content according to § 55 Abs. 2 RStV:</strong><br>
John Doe <br> 
Smith Main<br> 
12345 City <br></p> 
<br> 
Website Imprint created by <a href="http://www.impressum-generator.de">impressum-generator.de</a>
 </div>');
?>
